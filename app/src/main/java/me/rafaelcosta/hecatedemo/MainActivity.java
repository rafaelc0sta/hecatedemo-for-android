package me.rafaelcosta.hecatedemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {
    Button button_signIn;
    TextView textView_info;

    String accessToken;
    String username;
    String userID;
    String application_id = "1";
    String application_secret = "CYxFUTlmOkxnUsDYVo9PXaO0YK0d?gGp";

    HecateAPI hecate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        OkHttpClient client = new OkHttpClient();
        client.setSslSocketFactory(CACertHelper.sslContextForTrustedCertificates(getResources().openRawResource(R.raw.ca_cert)).getSocketFactory());


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.rafaelcosta.me:8079")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        hecate = retrofit.create(HecateAPI.class);

        button_signIn = (Button)this.findViewById(R.id.button);
        textView_info = (TextView)this.findViewById(R.id.textView);

        button_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hecate.fetchRequestToken(application_id, application_secret).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Response<JsonObject> response, Retrofit retrofit) {
                        Intent browserIntent = null;
                        Log.i("RAFAX", "JSON: " + response.body().toString());

                        try {
                            JSONObject json = new JSONObject(response.body().toString());
                            Log.i("RAFAX", "URL: " + "http://api.rafaelcosta.me/fmp/?requestToken=" + json.getString("Request Token"));
                            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://api.rafaelcosta.me/fmp/?requestToken=" + json.getString("Request Token")));
                            startActivity(browserIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.i("RAFAX", "err: " + t.getLocalizedMessage());
                        Toast.makeText(getApplicationContext(), "Error retrieving request token: " + t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    }
                });

//                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://api.rafaelcosta.me:8079/auth/requestToken", body, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Intent browserIntent = null;
//                        Log.i("RAFAX", "JSON: " + response.toString());
//                        try {
//                            Log.i("RAFAX", "URL: " + "http://api.rafaelcosta.me/fmp/?requestToken=" + response.getString("Request Token"));
//                            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://api.rafaelcosta.me/fmp/?requestToken=" + response.getString("Request Token")));
//                            startActivity(browserIntent);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getApplicationContext(), "Error retrieving request token: " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
//                    }
//                }) {
//                    @Override
//                    public String getBodyContentType() {
//                        return "application/x-www-form-urlencoded; charset=UTF-8";
//                    }
//                };
//
//                queue.add(request);
            }
        });


        Intent intent = null;

        if (getIntent() != null) {
            intent = getIntent();
            String action = intent.getAction();
            Uri data = intent.getData();
            if (Intent.ACTION_VIEW.equals(action) && data != null) {
                accessToken = data.getQueryParameter("accessToken");
                username = data.getQueryParameter("username");

                Toast.makeText(getApplicationContext(), "Retrieved Username/AccessToken combination. Attempting to get user id!", Toast.LENGTH_LONG).show();

//                String body = "application_id=" + application_id + "&application_secret=" + application_secret;
//                body += "&username=" + username + "&accessToken=" + accessToken;
//
//                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://api.rafaelcosta.me:8079/auth/verify", body, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i("RAFAX", response.toString());
//
//                        try {
//                            textView_info.setText("Username: " + username + "\nUser ID: " + response.getString("User ID"));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(getApplicationContext(), "Error retrieving request token: " + error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
//                    }
//                }){
//                    @Override
//                    public String getBodyContentType() {
//                        return "application/x-www-form-urlencoded; charset=UTF-8";
//                    }
//                };
//                queue.add(request);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
