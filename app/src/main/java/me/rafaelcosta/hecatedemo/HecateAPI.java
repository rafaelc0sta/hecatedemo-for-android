package me.rafaelcosta.hecatedemo;

import com.google.gson.JsonObject;

import retrofit.Call;
import retrofit.http.*;

/**
 * Created by rafaelcosta on 11/9/15.
 */
public interface HecateAPI {
    @FormUrlEncoded
    @POST("/auth/requestToken")
    Call<JsonObject> fetchRequestToken(@Field("application_id") String applicationId, @Field("application_secret") String applicationSecret);
}
